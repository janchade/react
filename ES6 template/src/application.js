// export default class Application {
class Application {
    constructor(appContainer, btnAdd, firstName, lastName, phoneNumber) {
        this.people = [];
        this.appContainer = appContainer;
        this.btnAdd = btnAdd;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;

    }



    addPerson() {
        var firstNameValue = document.getElementById(this.firstName).value;
        var lastNameValue = document.getElementById(this.lastName).value;
        var phoneNumberValue = document.getElementById(this.phoneNumber).value;

        var id = 0;
        if (this.people.length > 0) {
            id = this.people[this.people.length - 1].id + 1;
        }

        this.people.push({
            id: id,
            firstName: firstNameValue,
            lastName: lastNameValue,
            phoneNumber: phoneNumberValue
        });

        this.render();
    };
    removePerson(sender) {
        var id = sender.target.dataset["id"];
        for (let index = 0; index < this.people.length; index++) {
            const person = this.people[index];
            if (person.id == id) {
                this.people.splice(index, 1);
                break;
            }
        }
        this.render();

    };

    render() {
        let container = document.getElementById(this.appContainer);
        let html = '<table class="table">' +
            '<thead><tr><th>Id</th><th>First Name</th><th>Last Name</th><th>Phone number</th><th></th></tr></thead' +
            '<tbody>';
        for (let index = 0; index < this.people.length; index++) {
            const person = this.people[index];
            html += `<tr>
                <td> ${index}</td>
                <td> ${person.firstName} </td>
                <td> ${person.lastName} </td>
                <td> ${person.phoneNumber} </td>
                <td><button class=btn-remove data-id= "${person.id}" >Remove</button></td>
                <td><button class=btn-edit data-id= "${person.id}" >Edit</button></td>
                </tr>`;

            document.getElementById(this.firstName).value = null;
            document.getElementById(this.lastName).value = null;
            document.getElementById(this.phoneNumber).value = null;
        }

        html += '</tbody></table>';
        container.innerHTML = html;
        let buttons = document.getElementsByClassName("btn-remove");
        for (let index = 0; index < buttons.length; index++) {
            const element = buttons[index];
            element.onclick = this.removePerson.bind(this);

        }
    };
    run() {
        this.render();
        document.getElementById(this.btnAdd).onclick = this.addPerson.bind(this);
    };

    // toggleHoverOn() {
    //     document.getElementById(this.firstName).style.backgroundColor = "black";
    //     console.log("toggleOn");
    // }
    // toggleHoverOff() {
    //     document.getElementById(this.firstName).style.backgroundColor = "white";
    //     console.log("toggleOff");
    // }

}